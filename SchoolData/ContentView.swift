//
//  ContentView.swift
//  SchoolData
//
//  Created by Saivarun Kandagatla on 2/14/24.
//

import Combine
import SwiftUI

struct ContentView: View {
    @StateObject var viewModoel = SchoolViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                if viewModoel.schools.isEmpty {
                    ProgressView()
                } else {
                    List(viewModoel.schools) { school in
                        NavigationLink {
                            SchoolDetailsView(school: school)
                        } label: {
                            Text(school.school_name ?? "No school name")
                        }
                    }
                }
            }
            .navigationTitle("Schools")
        }
        .task {
            await viewModoel.fetchSchools()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
