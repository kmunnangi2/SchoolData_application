//
//  School.swift
//  SchoolData
//
//  Created by Saivarun Kandagatla on 2/14/24.
//

import Foundation

struct School: Codable, Identifiable, Equatable {
    let school_name: String?
    let overview_paragraph: String?
    
    var id: String {
        school_name ?? UUID().uuidString // TODO: should not rely on UUID
    }
}
