//
//  SchoolDataAPIClient.swift
//  SchoolData
//
//  Created by Saivarun Kandagatla on 2/14/24.
//

import Foundation


enum APIError: Error {
    case invalidURL
    case dataError
}

protocol SchoolDataAPIClientProtocol {
    func fetchSchools() async throws -> [School]
}
class SchoolDataAPIClient: SchoolDataAPIClientProtocol {
    private let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func fetchSchools() async throws -> [School] {
        guard let url = URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json") else {
            throw APIError.invalidURL
        }
        
        let (data, response) = try await session.data(from: url)
        
        guard let httpsReponse = response as? HTTPURLResponse, 200..<300 ~= httpsReponse.statusCode else {
            throw APIError.dataError
        }
        
        do {
            let schools = try JSONDecoder().decode([School].self, from: data)
            return schools
        } catch {
            throw APIError.dataError
        }
    }
}
