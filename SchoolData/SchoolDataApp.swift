//
//  SchoolDataApp.swift
//  SchoolData
//
//  Created by Saivarun Kandagatla on 2/14/24.
//

import SwiftUI

@main
struct SchoolDataApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
