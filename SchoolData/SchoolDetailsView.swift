//
//  SchoolDetailsView.swift
//  SchoolData
//
//  Created by Saivarun Kandagatla on 2/14/24.
//

import Foundation
import SwiftUI

struct SchoolDetailsView: View {
    let school: School
    
    var body: some View {
        VStack {
            Text(school.overview_paragraph ?? "N/A")
                .padding()
            
            Spacer()
        }
        .navigationTitle(school.school_name ?? "")
        .navigationBarTitleDisplayMode(.inline)
    }
}
