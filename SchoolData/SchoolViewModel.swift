//
//  SchoolViewModel.swift
//  SchoolData
//
//  Created by Saivarun Kandagatla on 2/14/24.
//

import Foundation

class SchoolViewModel: ObservableObject {
    @Published var schools: [School] = []
    private let apiClient: SchoolDataAPIClientProtocol
    
    init(apiClient: SchoolDataAPIClientProtocol = SchoolDataAPIClient()) {
        self.apiClient = apiClient
    }
    func fetchSchools() async {
        do {
            let schools = try await apiClient.fetchSchools()
            DispatchQueue.main.async {
                self.schools = schools
            }
        } catch {
            // TODO: handler error later
            self.schools = []
        }
    }
}
