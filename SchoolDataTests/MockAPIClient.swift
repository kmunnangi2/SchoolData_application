//
//  MockAPIClient.swift
//  SchoolDataTests
//
//  Created by Saivarun Kandagatla on 2/14/24.
//

import Foundation
@testable import SchoolData


class MockAPIClient: SchoolDataAPIClientProtocol {
    let schols: [School]?
    let error: APIError?
    
    init(schols: [School]? = nil, error: APIError? = nil) {
        self.schols = schols
        self.error = error
    }
    
    func fetchSchools() async throws -> [School] {
        if let error = error {
            throw error
        }
        
        if let validSchols = schols {
            return validSchols
        }
        
        fatalError("School and error are nil")
    }
}
