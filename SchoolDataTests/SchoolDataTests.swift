//
//  SchoolDataTests.swift
//  SchoolDataTests
//
//  Created by Saivarun Kandagatla on 2/14/24.
//

import XCTest
@testable import SchoolData

class SchoolViewModelTest: XCTestCase {
    func testFetchSchoolSuccess() async throws {
        let schools = [School(school_name: "Random name", overview_paragraph: "paragraph")]
        let mockAPIClient = MockAPIClient(schols: schools)
        let viewModel = SchoolViewModel(apiClient: mockAPIClient)
        
        await viewModel.fetchSchools()
        
        let exp = expectation(description: #function)
        let result = XCTWaiter.wait(for: [exp], timeout: 2.0)
        
        if result == XCTWaiter.Result.timedOut {
            XCTAssertEqual(viewModel.schools, schools)
        }
    }
    
    func testFetchSchoolFailure() async throws {
        let mockAPIClient = MockAPIClient(error: .dataError)
        let viewModel = SchoolViewModel(apiClient: mockAPIClient)
        
        await viewModel.fetchSchools()
        
        let exp = expectation(description: #function)
        let result = XCTWaiter.wait(for: [exp], timeout: 2.0)
        
        if result == XCTWaiter.Result.timedOut {
            XCTAssertTrue(viewModel.schools.isEmpty)
        }
    }
}
